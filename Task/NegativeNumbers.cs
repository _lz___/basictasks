﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class NegativeNumbers
    {
        public static void Start()
        {
            Console.Write("Количество строк: ");
            Int32.TryParse(Console.ReadLine(), out int n);
            Console.Write("Количество столбцов: ");
            Int32.TryParse(Console.ReadLine(), out int m);
            int[,] array = new int[n, m];

            Random rand = new Random();
            int count = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    array[i, j] = rand.Next(-5, 15);
                    if (array[i, j] < 0)
                    {
                        count++;
                    }
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine($"Количество отрицательных элементов: {count}");
        }
    }
}
