﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class WordCount
    {
        public static void Start()
        {
            Console.WriteLine("Введите текст.");
            string text = Console.ReadLine();

            string[] textArray = text.Split(new char[] { ' ' });
            Console.WriteLine("Кол-во слов в тексте: " + textArray.Length);
        }
    }
}
