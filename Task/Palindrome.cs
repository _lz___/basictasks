﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class Palindrome
    {
        public static void Start ()
        {
            bool test = true;
            int index;
            Console.Write("Введите слово: ");
            string word = Console.ReadLine();
            word = word.ToLower();
            char[] word2 = word.ToCharArray();
            if (word2.Length % 2 == 0)
                index = word2.Length / 2;
            else
                index = word2.Length / 2 + 1;
            for (int i = 0, j = word2.Length - 1; i < index; i++, j--)
            {
                if (word2[i] == word2[j])
                    continue;
                else
                {
                    test = false;
                    break;
                }
            }
            if (test)
                Console.WriteLine("Cлово {0} является палиндромом", word);
            else
                Console.WriteLine("Cлово {0} не является палиндромом", word);
            Console.ReadKey();
        }
    }
}
