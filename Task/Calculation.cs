﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class Calculation
    {
        public static void Start()
        {
            int[,] array = new int[4, 4];

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write("массив[" + i + "," + j + "]: ");
                    array[i, j] = int.Parse(Console.ReadLine());
                }
            }
            Console.WriteLine();

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write(" массив[" + i + "," + j + "]: " + array[i, j] + " ");
                }
                Console.WriteLine();
            }

            for (int i = 0; i < 4; i++)
            {
                int sum = 0;
                for (int j = 0; j < 4; j++)
                {
                    sum += array[i, j];
                }
                Console.WriteLine("Сумма строки: " + sum);
            }

            for (int i = 0; i < 4; i++)
            {
                int sum = 0;
                for (int j = 0; j < 4; j++)
                {
                    sum += array[j, i];
                }
                Console.WriteLine("Сумма столбца: " + sum);
            }

            int max = 0;
            for (int i = 0; i < 4; i++)
            {         
                for (int j = 0; j < 4; j++)
                {
                    if (i == j)
                    {
                        if (max <= array[i, j])
                            max = array[i, j];
                    }
                }
            }
            Console.WriteLine("Mаксимальный элемент в диагонал: " + max);
        }
    }
}
