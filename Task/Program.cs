﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("1)Процент вхождения заданного символа в строку.");
            Console.WriteLine("2)Найти суммы элементов каждой строки,столбца и max элемент диагонали");
            Console.WriteLine("3)Кол-во слов в строчке.");
            Console.WriteLine("4)Сортировка четность/нечетность");
            Console.WriteLine("5)Разность между максимальным и минимальным числом.");
            Console.WriteLine("6)Количество элементов, удовлетворяющих условию 0 < M[i] <125.");
            Console.WriteLine("7)Палиндром строки.");
            Console.WriteLine("8)Отрицательные числа в массиве.");
            Console.WriteLine("9)Количество элементов, удовлетворяющих условию -100 < M[i] < 100.");
            //Console.WriteLine("10)Сравнение массива.");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1: /*CharacterSearch.Scan();*/
                        CharacterSearch.BestCode();
                    break;
                case 2: Calculation.Start();
                    break;
                case 3: WordCount.Start();
                    break;
                case 4: ArraySorting.Start();
                    break;
                case 5: DifferenceOfNumbers.Start();
                    break;
                case 6: NumberOfElements.Start();
                    break;
                case 7: Palindrome.Start();
                    break;
                case 8:
                    NegativeNumbers.Start();
                    break;
                case 9:
                    RangeNumber.Start();
                    break;
                //case 10: ArrayComparison.Start();
                //    break;
            }

            Console.ReadKey();
        }
    }
}
