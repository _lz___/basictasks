﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class CharacterSearch
    {
        public static void Scan()
        {
            Console.WriteLine("Введите текст.");
            string text = Console.ReadLine();

            Console.WriteLine("Введите букву");
            string letter = Console.ReadLine();

            int indexOfChar = text.Where(a => a.ToString() == letter).ToList().Count(); 

            //for (int i = 0; i < text.Length; i++)
            //{       
            //  
            //}

            Console.WriteLine("Буква '" + letter + "' встречается " + indexOfChar + " раз(а).");

            int number = text.Length;
            Console.WriteLine("Всего в строчке символов: " + number);

            float percent = (indexOfChar * 100) / number;

            Console.WriteLine("Процент от строчки : " + percent + "%");
        }

        public static void BestCode()
        {

            Console.WriteLine("Введите строку");
            string text = Console.ReadLine();

            Console.WriteLine("Введите букву");
            char letter = Console.ReadLine()[0];

            int count = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == letter)
                    count++;
            }

            Console.WriteLine((float)count / text.Length * 100);
            Console.ReadLine();
        }
    }
}
