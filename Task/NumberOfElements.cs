﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class NumberOfElements
    {
        public static void Start()
        {
            Random random = new Random();
            int[] array = new int[100];

            for (int i= 0; i < array.Length; i++)
            {
                array[i] = random.Next(-100, 100);
            }

            int index = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (0 < array[i] && array[i] < 125)
                {
                    index++;
                }
            }
            Console.WriteLine("Количество элементов, удовлетворяющих условию: " + index);
        }
    }
}
